Signalista
==========

Signalista is simple proof of concept project that ties together backend eventing with a web frontend. Some of the more intersting features are

  - Simple pub/sub model allows frontend to be aware of backend changes
  - SignalR used to connect HTML frontend 
  - Messages filtered to respective receiver

This project started out as way to research a way to notify frontend clients of changes in database, with a little bit more finesse than polling. 

Components
----------

There are four main components 

 - Client.Web, SignalR hub, event subscriber and SignalR client
 - Client.Console, SignalR client
 - Worker, NServiceBus host
 - SecurityMonitor, SignalR client and event publisher

The "security monitor" is basically a simple console application that uses NServiceBus to publish events.


Requirements
------------
NServiceBus uses MSMQ as a queue so you need to install that before firing the project up.

