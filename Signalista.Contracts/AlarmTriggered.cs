﻿using System;
using NServiceBus;

namespace Signalista.Contracts
{
    public class AlarmTriggered : IEvent
    {
        public string Message { get; set; }
        public int CustomerId { get; set; }
    }
}
