﻿using System;
using NServiceBus;
using NServiceBus.Installation.Environments;
using Signalista.Contracts;

namespace Signalista.SecurityMonitor.Console
{
    class Program
    {
        private static IBus _bus;
        
        static void Main(string[] args)
        {
            InitBus();

            System.Console.WriteLine("Security monitor. Trigger alarms with 'alarm1' and 'alarm2'.");

            var line = String.Empty;
            while ((line = System.Console.ReadLine()) != null)
            {
                // Send a message to the server
                if (line.ToLower() == "alarm1")
                {
                    RaiseAlarm("Alarm 1 triggered", 1);
                }
                else if(line.ToLower() == "alarm2")
                {
                    RaiseAlarm("Alarm 2 triggered", 2);
                }
            }
        }

        private static void RaiseAlarm(string message, int customerId)
        {
            var msg = new AlarmTriggered() { Message = message, CustomerId = customerId };
            _bus.Publish(msg);
        }

        private static void InitBus()
        {
            var config  = Configure.With()
                    .Log4Net()
                    .DefineEndpointName("Signalista.DataWorker")
                    .DefaultBuilder()
                    .JsonSerializer()
                    .MsmqTransport()
                        .IsTransactional(true)
                    .UnicastBus()
                    .MsmqSubscriptionStorage();

            _bus = config
                .CreateBus()
                .Start(() => Configure.Instance.ForInstallationOn<Windows>().Install());
        }
    }
}
