﻿using Microsoft.AspNet.SignalR;
using NServiceBus;
using Signalista.Contracts;

namespace Signalista.Client.Web
{
    public class SimpleHub : Hub, IHandleMessages<AlarmTriggered>
    {
        public void Join(string groupName)
        {
            Groups.Add(Context.ConnectionId, groupName);
        }

        public void Send(string message)
        {
            Clients.Others.addMessage(message);    
        }

        public void Handle(AlarmTriggered message)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<SimpleHub>();
            var group = context.Clients.Group(message.CustomerId.ToString());
            group.alarmRaised(message.Message);
        }
    }
}