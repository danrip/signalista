﻿using System;
using System.Web.Routing;
using NServiceBus;
using NServiceBus.Installation.Environments;

namespace Signalista.Client.Web
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            RouteTable.Routes.MapHubs();
            RegisterAsSubscriber();
        }


        private void RegisterAsSubscriber()
        {
            // NServiceBus configuration
            Configure.With()
                     .DefineEndpointName("Signalista.Client.Web")
                     .DefaultBuilder()
                     .JsonSerializer()
                     .MsmqTransport()
                        .IsTransactional(true)
                        .PurgeOnStartup(false)
                     .UnicastBus()
                     .ImpersonateSender(false)
                     .LoadMessageHandlers()
                     .CreateBus()
                     .Start(() => Configure.Instance.ForInstallationOn<Windows>().Install());
        }
    }
}