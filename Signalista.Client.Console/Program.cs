﻿using System;
using Microsoft.AspNet.SignalR.Client.Hubs;
using NServiceBus;
using NServiceBus.Installation.Environments;

namespace Signalista.Client.Console
{
    public class Program 
    {
        static void Main(string[] args)
        {
            InitBus();

           // Connect to the service
            var hubConnection = new HubConnection("http://localhost:53151/");

            // Create a proxy to the chat service
            var chat = hubConnection.CreateHubProxy("simplehub");

            // Print the message when it comes in
            chat.On("addMessage", msg => System.Console.WriteLine(msg));

            // Start the connection
            hubConnection.Start().Wait();

            System.Console.WriteLine("Type anything for simple chatting with the web client.");

            var line = String.Empty;
            while ((line = System.Console.ReadLine()) != null)
            {
               chat.Invoke("Send", line).Wait();
            }
        }

        public static void InitBus()
        {
            Configure.With()
                  .DefineEndpointName("Signalista.Client.Console")
                  .DefaultBuilder()
                  .JsonSerializer()
                  .MsmqTransport()
                     .IsTransactional(true)
                     .PurgeOnStartup(false)
                  .UnicastBus()
                  .ImpersonateSender(false)
                  .LoadMessageHandlers()
                  .CreateBus()
                  .Start(() => Configure.Instance.ForInstallationOn<Windows>().Install());
        }
    }
}
    