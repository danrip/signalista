﻿using NServiceBus;
using Signalista.Contracts;

namespace Signalista.Client.Console
{
    public class MessageHandler : IHandleMessages<AlarmTriggered>
    {
        public void Handle(AlarmTriggered message)
        {
            System.Console.WriteLine("I also subscribe to this message : " + message.Message);
        }
    }
}
