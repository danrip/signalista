﻿
using NServiceBus;
using NServiceBus.Installation.Environments;

namespace Signalista.Worker
{
    public class DataWorker : IConfigureThisEndpoint
                            , AsA_Server
                            , IWantCustomInitialization
    {
        protected IBus Bus { get; set; }
        
        public void Init()
        {
            InitBus();
        }

        private void InitBus()
        {
            Configure.With().DefineEndpointName("Signalista.DataWorker")
                    .DefaultBuilder()
                    .JsonSerializer()
                    .MsmqTransport()
                       .IsTransactional(true)
                       .PurgeOnStartup(false)
                    .UnicastBus()
                    .ImpersonateSender(false)
                    .MsmqSubscriptionStorage()
                    .CreateBus()
                    .Start(() => Configure.Instance.ForInstallationOn<Windows>().Install());
        }
    }
}
